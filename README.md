# Odoo Scripts Collection

## DATA SHEET

---

[Odooerpcloud 13 bash script](https://odooerpcloud.com/)

[Yenthe666 13 bash script](https://github.com/Yenthe666/InstallScript)

### Enviroments

Tested on Ubuntu 18.04, 20.04 LTS

## INSTALL

---

Scripts include [Odoo](https://nightly.odoo.com/), [Python](https://www.python.org/), [PostgreSQL](https://www.postgresql.org/download/), libxml2, libxslt1, libevent, libsasl2 and libldap2. For more information see [Odoo Dependencies](https://www.odoo.com/documentation/13.0/setup/install.html#:~:text=For%20libraries%20using%20native%20code,%2C%20libevent%2C%20libsasl2%20and%20libldap2.&text=Odoo%20dependencies%20are%20listed%20in%20the%20requirements.,-txt%20file%20located)

### Basic Odooerpcloud

```bash
wget https://gitlab.com/maravento/odooscripts/-/raw/master/odooerpcloud/odoo13/odoo13.sh
chmod +x odoo13.sh
./odoo13.sh
```

### Alternative Yenthe666

```bash
wget https://gitlab.com/maravento/odooscripts/-/raw/master/odooyenthe666/odoo13/odoo_install_13.sh
chmod +x odoo_install_13.sh
./odoo_install_13.sh
```

## SERVICE

---

```bash
systemctl status odooxx # shows state Odoo service
systemctl start odooxx # start service
systemctl stop odooxx # stop service
systemctl restart odooxx # restart service
tail -f /opt/deploy/log/ # Shows Live Odoo Log
```

## LOCATION

---

```bash
/opt/odoosrc # All Source
/opt/odoosrc/extra-addons # Extra Addons
/opt/odoosrc/extra-addons/oca # Put here your OCA Extra Addons
/opt/odoosrc/data # Filestore
/opt/odoosrc/log/ # Log file
```

## DISCLAIMER

---

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
