#!/bin/bash
#################################################################
# (c) 2017. Ratio Server Project                                #
# Ratio is a Server with Odoo                                   #
# Install: git clone https://github.com/maravento/ratio.git     #
# chmod +x ratio.sh && ./ratio.sh                               #
# by Maravento.com. Based on Yenthe Van Ginneken Odoo script    #
# https://github.com/Yenthe666/InstallScript                    #
#################################################################
#
# Language spa-eng
#
cm1=("Sistema x86. Recomendado para VMs" "x86 System. Recommended for VMs")
cm2=("Sistema x64. No recomendado para VMs" "x64 System. Not recommended for VMs")
cm3=("SO Incorrecto. Instalacion Abortada" "OS Incorrect. Installation Aborted")
cm4=("Asegurese de tener instalado Ubuntu 16.04.x LTS x86" "Be sure to have installed Ubuntu 16.04.x LTS x86")
cm5=("Introduzca" "Enter")
cm6=("Formato de Net interfaces Correcto" "Net Interfaces Format Correct")
cm7=("Formato de Net Interfaces Incorrecto" "Net Interfaces Format Incorrect")
cm8=("Lista de Net Interfaces detectadas con direcciones MACs" "List of Net Interfaces detected with MACs addresses")
cm9=("Ha terminado la configuracion de Net Interfaces" "You have finished setting up your Net Interfaces")
cm10=("Reinicie su servidor y ejecute nuevamente gateproxy.sh" "Restart your server and run again ratio.sh")
cm11=("Ratio trabaja con NIC-Ethernet. Si eligió una interfaz WiFi" "Ratio works with Ethernet NIC. If you chose a WiFi interface")
cm12=("edite /etc/udev/rules.d/10-network.rules antes de reiniciar su" "edit /etc/udev/rules.d/10-network.rules before restarting your")
cm13=("servidor, y en KERNEL de interfaz WiFi, reemplace: en* por wl*" "server, and KERNEL WiFi interface, replace: in * for wl *")
cm14=("Desea eliminar" "Do you want to purge")
cm15=("Desea instalar" "Do you want to install")
cm16=("Por favor responda" "Please Answer")

test "${LANG:0:2}" == "es"
es=$?

clear

function x64x86(){
ARCHITECTURE=`uname -m`
	if [ ${ARCHITECTURE} == 'i686' ]; then # or x86_64
	echo "${cm1[${es}]}"
  else
	echo "${cm2[${es}]}"
	echo
fi
}
x64x86

# CHECKING SO
function is_xenial(){
is_xenial=`lsb_release -sc | grep xenial`
	if [ "$is_xenial" ]; then
    echo
	echo "Ubuntu 16.04 Xenial OK"
	echo
  else
	clear
	echo
	echo
	echo "${cm3[${es}]}"
	echo "${cm4[${es}]}"
	echo
	exit
fi
}
is_xenial

# CHECKING INTERFACES
# MAC ADDRESS/ETH PUBLIC
function is_mac_public(){
	read -p "${cm5[${es}]} MAC eth0 (Internet): " MAC
	if [ "$MAC" ]; then
	sed -i "s/00:00:00:00:00:00/$MAC/g" 10-network.rules
   fi
}

# MAC ADDRESS/ETH LOCAL
function is_mac_local(){
	read -p "${cm5[${es}]} MAC eth1 (Localnet): " MAC
	if [ "$MAC" ]; then
	sed -i "s/11:11:11:11:11:11/$MAC/g" 10-network.rules
   fi
}

cat <<EOF > 10-network.rules
# Archivo de Configuracion persistente ETH
# Ubicacion /etc/udev/rules.d/10-network.rules
# ip link | awk '/ether/ {print $2}' or cat /sys/class/net/*/address or ifconfig | grep HW or ifconfig | grep HW | awk '{print $5}'
# Public eth0
SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ATTR{address}=="00:00:00:00:00:00", ATTR{dev_id}=="0x0", ATTR{type}=="1", KERNEL=="en*", NAME="eth0"
# Local Net eth1
SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ATTR{address}=="11:11:11:11:11:11", ATTR{dev_id}=="0x0", ATTR{type}=="1", KERNEL=="en*", NAME="eth1"
EOF

function is_interfaces(){
is_interfaces=`ifconfig | grep eth`
	if [ "$is_interfaces" ]; then
	echo
	echo "${cm6[${es}]}"
	sudo rm -rf 10-network.rules
	echo
  else
	echo
	echo "${cm7[${es}]}"
	echo "${cm8[${es}]}:"
	echo
	ifconfig | grep HW
	echo
	is_mac_public
	is_mac_local
	sudo cp 10-network.rules /etc/udev/rules.d/10-network.rules
	sudo rm -rf 10-network.rules
	clear
	echo	
	echo "${cm9[${es}]}"
	echo "${cm10[${es}]}"
	echo
	echo "Importante - Important:"
	echo "${cm11[${es}]}"
	echo "${cm12[${es}]}"
	echo "${cm13[${es}]}"
	echo
	exit 
  fi
}
is_interfaces

clear
echo
echo "    Bienvenido a la instalacion de Ratio Server v1.0"
echo "        Welcome to installing Ratio Server v1.0     "
echo
echo "  Requisitos Mínimos / Minimum requirements:"
echo "  GNU/Linux:    Ubuntu Mate 16.04.x LTS x86 VM"
echo "  Processor:    Intel compatible 1x GHz"
echo "  Interfaces:   eth0, eth1"
echo "  RAM:          3GB"
echo "  DD:           100 GB"
echo
echo "  Exención de responsabilidad / Disclaimer:
  Este script puede dañar su sistema si se usa incorrectamente
  Para mayor información, visite gateproxy.com y lea el HowTO
  This script can damage your system if used incorrectly
  For more information, visit maravento.com and read the HowTO"
echo
echo "  Presione ENTER para iniciar o CTRL+C para cancelar
  Press ENTER to start or CTRL+C to cancel";
read RES

#--------------------------------------------------
# synchronized
#--------------------------------------------------

sudo killall apt-get >/dev/null 2>&1
sudo hwclock -w >/dev/null 2>&1
sudo cp /etc/crontab{,.bak} >/dev/null 2>&1
sudo crontab /etc/crontab >/dev/null 2>&1
sudo cp /etc/apt/sources.list{,.bak} >/dev/null 2>&1
sudo apt-get -y install git apt dpkg wget tar
sudo update-desktop-database
sudo apt-get -y install linux-headers-$(uname -r)
sudo update-initramfs -d -u
sudo apt-get -y autoremove
sudo update-grub
sudo hdparm -W /dev/sda
sudo add-apt-repository ppa:eugenesan/ppa -y
sudo add-apt-repository ppa:ubuntu-x-swat/updates -y
sudo dpkg --add-architecture i386
sudo apt-get -f -y install
sudo service sendmail stop >/dev/null 2>&1 && sudo update-rc.d -f sendmail remove
sudo sh -c 'printf "[Seat:*]\nallow-guest=false\n" >/etc/lightdm/lightdm.conf.d/50-no-guest.conf'
echo OK
sleep 2

#--------------------------------------------------
# Purge
#--------------------------------------------------
function is_purge(){
clear
echo
while true; do
	read -p "${cm14[${es}]} aplicaciones no esenciales? (y/n)" answer
		case $answer in
          [Yy]* )
		# execute command yes
	sudo apt-get -y remove --purge libreoffice* vlc thunderbird cheese brasero rhythmbox hexchat tilda pidgin && sudo apt clean && sudo apt-get -y autoremove
	echo OK
	sleep 2
			break;;
          	[Nn]* )
		# execute command no
			break;;
        * ) echo; echo "${cm16[${es}]}: YES (y) or NO (n)";;
    esac
done
}
is_purge

#--------------------------------------------------
# Mate Desktop
#--------------------------------------------------

function is_mate(){
# Mate Desktop
clear
echo

is_mate=`which mate-panel`
    if [ "$is_mate" ]; then
        echo "Mate Desktop Ok"
    else
        while true; do
        read -p "${cm15[${es}]} Mate Desktop? (y/n)" answer
    	case $answer in
          [Yy]* )
		# execute command yes
	sudo add-apt-repository ppa:ubuntu-mate-dev/$(lsb_release -sc)-mate --yes
	updateandclean
	sudo apt-get -y install mate-core mate-desktop-environment mate-desktop-environment-extras mate-dock-applet mate-applets
	sudo apt-get -f -y install
	echo OK
	sleep 2
			break;;
          	[Nn]* )
		# execute command no
			break;;
        * ) echo; echo "${cm16[${es}]}: YES (y) or NO (n)";;
    esac
done
fi
}
is_mate

echo -e "* Create services file"
cat <<EOF > services.sh
#!/bin/bash
### BEGIN INIT INFO
# Provides:          services reload
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start daemon at boot time
# Description:       Enable service provided by daemon.
### END INIT INFO
EOF

#--------------------------------------------------
# Clean and Update
#--------------------------------------------------

updateandclean(){
clear
echo
echo "Su sistema se esta actualizando - Your system is being updated..."
sudo apt update && sleep 1 && sudo apt-get -y dist-upgrade && sleep 1 && sudo apt install --fix-missing -y && sleep 1 && sudo apt-get -f -y install && sudo fc-cache && sleep 1 && sudo sync && sleep 1 && sudo sysctl -w vm.drop_caches=3 vm.swappiness=10 && sleep 1 && sudo apt-get -y autoremove && sleep 1 && sudo apt-get -y autoclean && sleep 1 && sudo apt-get -y clean && sleep 1 && sudo dpkg --configure -a && sleep 1 && sudo apt-get -f -y install
}
updateandclean

#--------------------------------------------------
# Essential
#--------------------------------------------------
clear
echo
echo "Essential Pack setup..."
sudo apt-get -y install aptitude apt-show-versions acpi arj autoconf automake autotools-dev avahi-daemon bleachbit bluefish build-essential bzr bzrtools cabextract cdbs check checkinstall cifs-utils cmake curl dconf-editor dconf-tools debhelper deborphan devscripts dh-make dialog dkms dmidecode dmraid dpatch exfat-fuse exfat-utils fakeroot file-roller fslint gcc gdebi gdebi-core geany gedit git git-core gksu gparted gtkorphan gir1.2-gtop-2.0 gjs hardinfo hfsplus hfsprogs hfsutils hfsutils-tcltk ipcalc jfsutils kpartx libauthen-pam-perl libclass-dbi-mysql-perl libconfig-general-perl libdate-manip-perl libdbi-perl libgksu2-0 libglib2.0-0 libhttp-server-simple-perl libio-pty-perl libio-socket-ssl-perl libmailtools-perl libmime-lite-perl libnet-ssleay-perl libpam-runtime librrds-perl libsasl2-modules libtool libupnp-dev libwww-perl libxml-parser-perl libxml-simple-perl lintian linux-tools-common lshw-gtk lsscsi lynx mailutils make module-assistant mpack netmask ntfs-config ntfs-3g npm p7zip-full p7zip-rar patch patchutils perl postfix postfix-doc postfix-mysql ppa-purge prelink preload procps python python-cairo python-dev python-gi python-glade2 python-gobject python-gobject-2 python-gtk2 python-notify python-pcapy python-gpgme python-gobject-2-dbg python-gtk2-doc devhelp python-gdbm-dbg python-tk-dbg quilt quota quotatool rar rcconf reiser4progs reiserfsprogs rrdtool sharutils snapd subversion synaptic sysinfo sysv-rc-conf ttf-dejavu unace unrar unzip util-linux uudeview vim vmm w3m xfsprogs xsltproc xutils zenity zip freefilesync apt-transport-https libcurl3 libjpeg62 python-babel python-babel-localedata python-pybabel
sudo dpkg --configure -a
sudo m-a prepare
sudo apt-get -f -y install
sudo chmod 777 /var/lib/update-notifier/package-data-downloads/partial
sudo apt-get -y install ttf-mscorefonts-installer
# gdiskdump
sudo rm gdiskdump*.deb >/dev/null 2>&1 && sudo apt-get -y purge gdiskdump >/dev/null 2>&1
wget -c --retry-connrefused -t 0 https://launchpad.net/gdiskdump/trunk/0.8/+download/gdiskdump_0.8-1_all.deb
sudo dpkg -i gdiskdump_0.8-1_all.deb
sudo apt-get -f -y install
echo OK
# Teamviewer
sudo apt-get -y purge teamviewer* >/dev/null 2>&1
sudo dpkg -r teamviewer:i386 >/dev/null 2>&1
sudo rm -rf ~\.local\share\TeamViewer* >/dev/null 2>&1
sudo wget -c --retry-connrefused -t 0 http://download.teamviewer.com/download/teamviewer_i386.deb
sudo dpkg -i --force-depends teamviewer_i386.deb
sudo dpkg --configure -a
sudo apt-get -f -y install
echo '# Teamviewer service
	date=`date +%d/%m/%Y" "%H:%M:%S`
	if [[ `ps -A | grep teamviewerd` != "" ]];then
	echo -e "\nONLINE"
	else
	echo -e "\n"
	killall teamviewerd >/dev/null 2>&1
	teamviewer --daemon start
	echo "Teamviewer start $date" >> /var/log/syslog
	fi'>> services.sh
# syncthing
curl -s https://syncthing.net/release-key.txt | sudo apt-key add -
echo "deb https://apt.syncthing.net/ syncthing stable" | sudo tee /etc/apt/sources.list.d/syncthing.list
sudo apt update && sudo apt-get -y install syncthing
sudo systemctl enable syncthing@$USER.service && sudo systemctl start syncthing@$USER.service
wget https://raw.githubusercontent.com/syncthing/syncthing-gtk/master/icons/st-logo-128.ico -O "Imágenes"/syncthing.jpg
echo '# Syncthing service
	MYUSER="ratio"
	date=`date +%d/%m/%Y" "%H:%M:%S`
	if [[ `ps -A | grep syncthing` != "" ]];then
	echo -e "\nONLINE"
	else
	echo -e "\n"
	killall syncthing >/dev/null 2>&1
	systemctl start syncthing@$MYUSER.service
	echo "Syncthing start $date" >> /var/log/syslog
	fi'>> services.sh
function is_username(){
	is_user=`echo $USER`
	if [ "$is_user" ]; then
	find services.sh -type f -print0 | xargs -0 -I "{}" sed -i "s:ratio:$is_user:g"  "{}"
   fi
}
is_username
echo OK
echo "Access: https://localhost:8384/"
sleep 2
echo OK
echo "Netdata setup..."
sudo apt-get -y install zlib1g-dev uuid-dev libmnl-dev autogen pkg-config jq nodejs && sudo apt-get -f -y install
git clone https://github.com/firehol/netdata.git --depth=1
cd netdata && sudo ./netdata-installer.sh && cd
echo '# NetData Service
	date=`date +%d/%m/%Y" "%H:%M:%S`
	if [[ `ps -A | grep netdata` != "" ]];then
	echo -e "\nONLINE"
	else
	echo -e "\n"
	killall netdata >/dev/null 2>&1
	/usr/sbin/netdata
	echo "NetData start $date" >> /var/log/syslog
	fi'>> services.sh
echo OK
echo "Netdata: http://localhost:19999/"
echo
# ubuntu-tweak
sudo apt -y install python-apt python-xdg python-lxml python-aptdaemon python-aptdaemon.gtk3widgets python-defer python-compizconfig gir1.2-gconf-2.0
sudo rm ubuntu-tweak*.deb >/dev/null 2>&1 && sudo apt -y purge ubuntu-tweak >/dev/null 2>&1
wget -c --retry-connrefused -t 0 http://archive.getdeb.net/ubuntu/pool/apps/u/ubuntu-tweak/ubuntu-tweak_0.8.7-1~getdeb2~xenial_all.deb && sudo dpkg -i --force-depends ubuntu-tweak_0.8.7-1~getdeb2~xenial_all.deb && sudo apt-get -f -y install
sleep 2

#--------------------------------------------------
# Removing orphans
#--------------------------------------------------

sudo deborphan | xargs sudo apt-get -y remove --purge
sudo deborphan --guess-data | xargs sudo apt-get -y remove --purge
sudo dpkg --configure -a && sudo apt-get -f -y install
echo OK

#--------------------------------------------------
# Services Upload
#--------------------------------------------------

sudo cp services.sh /etc/init.d
sudo chown root:root /etc/init.d/services.sh
sudo chmod +x /etc/init.d/services.sh
echo "Services Crontab tasks..."
sudo crontab -l | { cat; echo "
*/05 * * * * /etc/init.d/services.sh"; } | sudo crontab -
sudo service cron restart
sleep 2
echo

#--------------------------------------------------
# Clean, Update, Reboot
#--------------------------------------------------

updateandclean
clear
echo
echo "Done. Presione ENTER para reiniciar - Press ENTER to reboot";
read RES
sudo reboot

#--------------------------------------------------
# Odoo
#--------------------------------------------------
clear
echo
echo "Odoo setup..." 
wget https://raw.githubusercontent.com/Yenthe666/InstallScript/10.0/odoo_install.sh && chmod +x odoo_install.sh && ./odoo_install.sh

wget -q -c --retry-connrefused -t 0 https://raw.githubusercontent.com/Programacion-con-Odoo-9/icono-odoo/master/unnamed.ico -O "Imágenes"/ratio.ico >/dev/null 2>&1

sudo nano /etc/odoo-server.conf
admin_passwd = UseStrongPassw0rd

localhost:8069

su - postgres
psql
update pg_database set datallowconn = TRUE where datname = 'template0';
\c template0
update pg_database set datistemplate = FALSE where datname = 'template1';
drop database template1;
create database template1 with template = template0 encoding = 'UTF8';
update pg_database set datistemplate = TRUE where datname = 'template1';
\c template1
update pg_database set datallowconn = FALSE where datname = 'template0';
\q
/etc/init.d/postgresql restart
/etc/init.d/odoo-server restart
echo '# Odoo service
	date=`date +%d/%m/%Y" "%H:%M:%S`
	if [[ `ps aux | grep python | grep odoo-server` != "" ]];then
	echo -e "\nONLINE"
	else
	echo -e "\n"
	pkill -9 -f openerp-server
	/etc/init.d/odoo-server start
	echo "Odoo Sever start $date" >> /var/log/syslog
	fi'>> services.sh
